/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul1.do_not_attempt_comparison_with_nan;

/**
 *
 * @author Habibi Imron
 */
public class NaNComparison {
    public static void main(String[] args) {
        // Noncompliant code
//        double x = 0.0;
//        double result = Math.cos(1/x); // returns NaN if input is infinity
//        if(result == Double.NaN){ // comparison is always false!
//            System.out.println("result is NaN");
//        }
        
        // Compliant code solution
        double x = 0.0;
        double result = Math.cos(1/x); // returns NaN if input is infinity
        if(Double.isNaN(result)){
            System.out.println("result is NaN");
        }
    }
}
