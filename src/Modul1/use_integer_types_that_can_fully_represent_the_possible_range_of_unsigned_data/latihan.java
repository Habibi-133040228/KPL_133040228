package Modul1.use_integer_types_that_can_fully_represent_the_possible_range_of_unsigned_data;


import java.io.DataInputStream;
import java.io.IOException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Habibi Imron
 */
public class latihan {
    // Noncompliant code
//    public static int getInteger(DataInputStream is) throws IOException{
//        return is.readInt();
//    }
    
    // Compliant code solution
    public static long getInteger(DataInputStream is) throws IOException{
        return is.readInt() & 0xFFFFFFFFL; //mask with 32 one-bit
    }
    

}
