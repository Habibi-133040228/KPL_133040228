/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul1.do_not_reuse_public_identifier_from_the_java_standard_library;

/**
 *
 * @author Habibi Imron
 */

/* Noncompliant Code
 * import java.util.Vector;
 * Kesalahan yang mungkin terjadi: 
 * kita mungkin berniat untuk mengimport class Vector yang kita buat sebelumnya
 * tetapi kita malah mengimport nama standar library yang sudah ada dikarenakana kemiripan nama apabila kita tidak teliti
 */ 


// Compliant Code 
import Modul1.do_not_reuse_public_identifier_from_the_java_standard_library.Vector;
public class VectorUser {
    public static void main(String[] args) {
        Vector v = new Vector ();
        if(v.isEmpty()){
            
            System.out.println("Vector is empty");
        }
    }
}
