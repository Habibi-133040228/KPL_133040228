/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul1.do_not_ignore_values_returned_by_methods;

import java.io.File;

/**
 *
 * @author Habibi Imron
 */
public class latihan4 {
    //Noncompliant Code
//    public void deleteFile(){
//        File someFile = new File("someFileName.txt");
//        // do something with file
//        someFile.delete();
//    }
    
    //Compliant Code Solution
    public void deleteFile(){
        File someFile = new File("someFileName.txt");
        // do something with file
        if(!someFile.delete()){
            System.out.println("File gagal untuk dihapus...");
        }
    }
    
   
    
}
