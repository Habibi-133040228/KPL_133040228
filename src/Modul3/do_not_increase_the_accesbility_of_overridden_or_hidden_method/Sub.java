/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_increase_the_accesbility_of_overridden_or_hidden_method;

/**
 *
 * @author Habibi Imron
 */
public class Sub extends Super{
    public void doLogic() {
        System.out.println("Sub invoked");
        // Do sensitive operations
    }
}
