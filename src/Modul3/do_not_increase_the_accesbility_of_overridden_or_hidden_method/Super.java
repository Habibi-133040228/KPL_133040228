/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_increase_the_accesbility_of_overridden_or_hidden_method;

/**
 *
 * @author Habibi Imron
 */
public class Super {
    // Noncompliant code
//    protected void doLogic() {
//        System.out.println("Super invoked");
//    }
    
    // Compliant code
    protected final void doLogic() { // Declare as final
        System.out.println("Super invoked");
        // Do sensitive operations
    }
}
