/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.do_not_suppress_or_ignore_checked_exceptions;

/**
 *
 * @author Habibi Imron
 */
class Foo implements Runnable {
    // Noncompliant code
//    public void run() {
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            // Ignore
//        }
//    }
    
    // Compliant code
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt(); // Reset interrupted status
        }
    }
}
