/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul3.ensure_that_constructors_do_not_call_overridable_method;

/**
 *
 * @author Habibi Imron
 */
public class SubClass extends SuperClass{
    private String color = null;
    public SubClass() {
        super();
        color = "Red";
    }
    
    public void doLogic() {
        System.out.println("This is subclass! The color is :" + color);
        // ...
    }
}
