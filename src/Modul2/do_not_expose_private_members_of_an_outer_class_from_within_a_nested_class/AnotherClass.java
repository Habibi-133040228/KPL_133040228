/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.do_not_expose_private_members_of_an_outer_class_from_within_a_nested_class;

/**
 *
 * @author Habibi Imron
 */
public class AnotherClass {
    // Noncompliant code
//    public static void main(String[] args) {
//        Coordinates c = new Coordinates();
//        Coordinates.Point p = c.new Point();
//        p.getPoint();
//    }
    
    // COmpliant code
    public static void main(String[] args) {
        Coordinates c = new Coordinates();
        Coordinates.Point p = c.new Point(); // Fails to compile
        p.getPoint();
    }
}
