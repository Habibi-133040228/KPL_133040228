package Modul2.use_compatible_character_encodings_when_communicating_string_data_between_jvms;


import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Habibi Imron
 */
public class latihan {
    // Noncompliant code
    public static void main(String[] args) {
//        FileInputStream fis = null;
//        try {
//            fis = new FileInputStream("SomeFile");
//            DataInputStream dis = new DataInputStream(fis);
//            byte[] data = new byte[1024];
//            dis.readFully(data);
//            String result = new String(data);
//        } catch (IOException x) { 
//            // Handle error
//        } finally {
//            if (fis != null) {
//                try {
//                    fis.close();
//                } catch (IOException x) {
//                    // Forward to handler
//                }
//            }
//        }
        
        // Compliant code solution
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("SomeFile");
            DataInputStream dis = new DataInputStream(fis);
            byte[] data = new byte[1024];
            dis.readFully(data);
            String result = new String(data, "UTF-16LE");
        } catch (IOException x) {
            // Handle error
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException x) {
                    // Forward to handler
                }
            }
        }
    }
    
    
}
