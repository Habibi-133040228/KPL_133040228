/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.do_not_assume_that_a_java_char_fully_represents_a_unicode_code_point;

/**
 *
 * @author Habibi Imron
 */
public class latihan {
    // Noncompliant code
//    public static String trim(String string){
//        char ch;
//        int i;
//        for (i = 0; i < string.length(); i += 1) {
//            ch = string.charAt(i);
//            if (!Character.isLetter(ch)) {
//                break;
//            }
//        }
//        return string.substring(i);
//    }
    
    // Compliant code solution
    public static String trim(String string) {
        int ch;
        int i;
        for (i = 0; i < string.length(); i += Character.charCount(ch)) {
            ch = string.codePointAt(i);
            if (!Character.isLetter(ch)) {
                break;
            }
        }
        return string.substring(i);
    }
}
