/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.be_wary_of_letting_constructors_throws_exception;

/**
 *
 * @author Habibi Imron
 */
public class Interceptor extends BankOperations {
    private static Interceptor stealInstance = null;
    
    public static Interceptor get() {
        try {
            new Interceptor();
        } catch (Exception ex) {/* Ignore exception */}
        try {
            synchronized (Interceptor.class) {
                while (stealInstance == null) {
                    System.gc();
                    Interceptor.class.wait(10);
                }
            }
        } catch (InterruptedException ex) { return null; }
        return stealInstance;
    }
    
    public void finalize() {
        synchronized (Interceptor.class) { 
            stealInstance = this;
            Interceptor.class.notify();
        }
        System.out.println("Stole the instance in finalize of " + this);
    }
}
