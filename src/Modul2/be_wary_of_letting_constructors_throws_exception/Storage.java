/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.be_wary_of_letting_constructors_throws_exception;

/**
 *
 * @author Habibi Imron
 */
public class Storage {
    private static BankOperations bop;
    public static void store(BankOperations bo) {
        // Store only if it is initialized
        if (bop == null) {
            if (bo == null) {
                System.out.println("Invalid object!");
                System.exit(1);
            }
            bop = bo;
        }
    }
}



