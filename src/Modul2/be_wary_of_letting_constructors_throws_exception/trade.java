/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2.be_wary_of_letting_constructors_throws_exception;

import java.io.IOException;

/**
 *
 * @author Habibi Imron
 */
public class trade {
    // Noncompliant code
//    private static Stock s;
    // Compliant code
    private static final Stock s;
    static {
        try {
            s = new Stock();
        } catch (IOException e) {
            /* Does not initialize s to a safe state */
        }
    }
    // ...
}
